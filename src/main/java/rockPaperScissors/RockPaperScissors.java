package rockPaperScissors;

import java.util.Arrays;
import java.util.List;
import java.util.Scanner;
import java.util.concurrent.ThreadLocalRandom;

public class RockPaperScissors {
	
    public static void main(String[] args) {
    	/* 	
    	 * The code here does two things:
    	 * It first creates a new RockPaperScissors -object with the
    	 * code `new RockPaperScissors()`. Then it calls the `run()`
    	 * method on the newly created object.
         */
        new RockPaperScissors().run();
    }
    
    Scanner sc = new Scanner(System.in);
    public static int roundCounter = 1;
    public static int humanScore = 0;
    public static int computerScore = 0;
    List<String> rpsChoices = Arrays.asList("rock", "paper", "scissors");
    
    
    public void run() {
        // TODO: Implement Rock Paper Scissors
        System.out.println(String.format("Let's play round " + roundCounter));
        String userChoice = userMove();
        String computerChoice = computerMove();
        winner(userChoice, computerChoice);
        System.out.println(String.format("Score: human %1s, computer %2s" , humanScore, computerScore));
        roundCounter += 1;
        if (keepPlaying()) {
            run();
        }
    }

    public static void winner(String move1, String move2) {
        if (result(move1, move2)) {
            humanScore += 1;
            System.out.println(String.format("Human chose %1s, computer chose %2s. Human wins!" ,move1, move2));
        }
        else if (result(move2, move1)) {
            computerScore += 1;
            System.out.println(String.format("Human chose %1s, computer chose %2s. Computer wins!" ,move1, move2));
        }
        else {
            System.out.println(String.format("Human chose %1s, computer chose %2s. It's a tie!" ,move1, move2));
        }        
    }

    public String userMove() {
        String user = readInput("Your choice (Rock/Paper/Scissors)?");
        if (rpsChoices.contains(user)) {
            return user;
        }
        else {
            System.out.println(String.format("I do not understand %s. Could you try again?" ,user));
            return userMove();
        }
    } 

    public String computerMove() {
        int rand = ThreadLocalRandom.current().nextInt(0, 3);
        String computer = rpsChoices.get(rand);
        return computer;
    }

    public static Boolean result(String move1, String move2) {
        if (move1.equals("paper")) {
            return (move2.equals("rock"));
        }
        else if (move1.equals("scissors")) {
            return (move2.equals("paper"));
        }
        else {
            return (move2.equals("scissors"));
        }
    }

    public Boolean keepPlaying() {        
        System.out.println("Do you wish to continue playing? (y/n)?");
        String answer = sc.next();
        if (answer.equals("y")) {
            return true;
        }
        else if (answer.equals("n")) {
            System.out.println("Bye bye :)");
            return false;
        }
        else {
            System.out.println(String.format("I do not understand %s. Could you try again?" ,answer));
            return keepPlaying();
        }
    }



    /**
     * Reads input from console with given prompt
     * @param prompt
     * @return string input answer from user
     */
    public String readInput(String prompt) {
        System.out.println(prompt);
        String userInput = sc.next();
        return userInput;
    }

}
